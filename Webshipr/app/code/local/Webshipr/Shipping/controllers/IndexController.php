<?php

class Webshipr_Shipping_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {

    }

    public function fulfillAction()
    {
        // Decode data, and validate
        $apiData = json_decode(file_get_contents('php://input', 'r'));

        /** @var Mage_Sales_Model_Order $order */
        $order = Mage::getModel('sales/order')->loadByIncrementId($apiData->order_id);

        if (!$apiData->api_token || $apiData->api_token != Mage::getStoreConfig('webshipr/webshipr/webshipr_token', $order->getStore()))
            die("Access denied");

        $comment = null;
        $email = true;
        $includeComment = false;
        $autoInvoice = Mage::getStoreConfigFlag('webshipr/webshipr/webshipr_auto_invoice', $order->getStore());

        $data = $order->getData();
        $orderId = $data["increment_id"];

        $webshiprOrder = $this->getAPI($order->getStore())->getOrder($orderId);


        // Create shipment
        $qtyToShip = array();

        foreach ($webshiprOrder->items as $webshiprItem) {
            if ($webshiprItem->is_fulfilled) {
                if (!isset($qtyToShip[$webshiprItem->ext_ref])) {
                    $qtyToShip[$webshiprItem->ext_ref] = 0;
                }

                $qtyToShip[$webshiprItem->ext_ref] += $webshiprItem->Quantity;
            }
        }
        foreach ($order->getAllItems() as $item) {
            if (array_key_exists($item["item_id"], $qtyToShip)) {
                $qtyToShip[$item["item_id"]] = $qtyToShip[$item["item_id"]] - $item->getQtyShipped();
            }
        }

        $shipment = $order->prepareShipment($qtyToShip);

        // Create tracking
        try {
            $trackingUrl = $apiData->tracking_url;
            $data = array();
            $data['carrier_code'] = 'custom';
            $data['title'] = $apiData->carrier_name;
            $data['number'] = $trackingUrl;
            $order->setData('webshipr_tracking_url', $trackingUrl);
            $track = Mage::getModel('sales/order_shipment_track')->addData($data);
            $shipment->addTrack($track);

            Mage::register('current_shipment', $shipment);

            $shipment->register();
            $shipment->addComment($comment, $email && $includeComment);
            $shipment->setEmailSent(true);
            $shipment->getOrder()->setIsInProcess(true);
        } catch (Exception $e) {
            $order->addStatusHistoryComment('Webshipr callback: Error creating tracking. Exception message: ' . $e->getMessage(), false);
            $order->save();
        }

        // Handle invoicing
        if ($autoInvoice) {
            try {
                $invoice = $order->prepareInvoice($qtyToShip);

                $invoice->setRequestedCaptureCase(Mage_Sales_Model_Order_Invoice::CAPTURE_ONLINE);

                $invoice->register();

                $invoice->getOrder()->setCustomerNoteNotify(true);
                $invoice->getOrder()->setIsInProcess(true);
                $invoice->sendEmail(true, false);
                $order->addStatusHistoryComment('Automatically invoiced by webshipr callback.', true);

                $transactionSave = Mage::getModel('core/resource_transaction')
                    ->addObject($invoice)
                    ->addObject($invoice->getOrder());

                $transactionSave->save();

                $order->setData('state', "complete");
                $order->setStatus("complete");
                $order->save();

            } catch (Exception $e) {
                $order->addStatusHistoryComment('Webshipr callback: Error creating invoice. Exception message: ' . $e->getMessage(), false);
                $order->save();
            }
        } else {

            $order->setData('state', "processing");
            $order->setStatus("processing");
            $order->save();
        }

        // End invoicing


        try {
            $transactionSave = Mage::getModel('core/resource_transaction')
                ->addObject($shipment)
                ->addObject($shipment->getOrder())
                ->save();

            $shipment->sendEmail($email, ($includeComment ? $comment : ''));

            $order->addStatusToHistory(Mage_Sales_Model_Order::STATE_COMPLETE);
        } catch (Exception $e) {
            $order->addStatusHistoryComment('Webshipr callback: Error saving shipment. Exception message: ' . $e->getMessage(), false);
            $order->save();
        }

        Mage::dispatchEvent('webshipr_order_fulfill_after', array(
            'order'          => $order,
            'callback'       => $apiData,
            'webshipr_order' => $webshiprOrder,
        ));

        echo (string)json_encode(array("result" => "ok"));
    }

    public function ajaxAction()
    {
        $isAjax = Mage::app()->getRequest()->isAjax();
        if ($isAjax) {
            $data = Mage::app()->getRequest()->getParams();

            $this->getResponse()->setHeader('Content-type', 'application/json');
            if ($data['ajaxToken'] !== Mage::getSingleton('checkout/session')->getWsAjaxToken()) {
                $responseText = json_encode(
                    array(
                        'error' => 'Invalid ajax token'
                    )
                );
                $this->getResponse()->setBody($responseText);
            } else {
                $api = $this->getAPI(Mage::app()->getStore());

                switch ($data["method"]) {
                    case 'getByZipCarrier':
                        echo json_encode($api->getShopsByCarrierAndZip($data["zip"], $data['carrier']));
                        break;
                    case 'getByZipRate':
                        echo json_encode($api->getShopsByRateAndZip($data["zip"], $data['rate_id']));
                        break;
                    case 'getByAddressRate':
                        echo json_encode($api->getShopsByRateAndAddress($data['address'], $data['zip'], 'DK', $data['rate_id']));
                        break;
                    default:
                        echo json_encode(array("error" => 'Method not defined'));
                        break;
                }
            }
        }
    }

    /**
     * @return Webshipr_Shipping_Model_WebshiprAPI
     */
    protected function getAPI(Mage_Core_Model_Store $store)
    {
        /** @var Webshipr_Shipping_Model_WebshiprAPI $apiModel */
        $apiModel = Mage::getModel('webshipr_shipping/webshiprAPI');
        $apiModel->init($store);

        return $apiModel;
    }
}
