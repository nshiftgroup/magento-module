<?php
include_once 'Structs.php';

class Webshipr_Shipping_Model_WebshiprGlue
{

    /**
     * @param Mage_Sales_Model_Order $magentoOrder
     * @param mixed                  $rateId
     * @return mixed
     */
    public static function reProcessOrder($magentoOrder, $rateId)
    {
        return self::_processOrder($magentoOrder, $rateId, true);
    }

    /**
     * @param Mage_Sales_Model_Order $magentoOrder
     * @param mixed                  $rateId
     * @return mixed
     */
    public static function processOrder($magentoOrder, $rateId)
    {
        return self::_processOrder($magentoOrder, $rateId, false);
    }

    /**
     * @param Mage_Sales_Model_Order $magentoOrder
     * @param mixed                  $rateId
     * @param boolean                $reProcess
     * @return mixed
     */
    protected static function _processOrder($magentoOrder, $rateId, $reProcess)
    {
        $data = $magentoOrder->getData();

        $mageBilling = $magentoOrder->getBillingAddress()->getData();
        $mageDelivery = $magentoOrder->getShippingAddress()->getData();
        $items = array();

        // Get weight unit from settings
        $weightUnit = Mage::getStoreConfig(
            'carriers/webshipr/weight_unit',
            $magentoOrder->getStore()
        );

        // Get items
        foreach (self::getShipmentItems($magentoOrder) as $item) {
            // Get additional variant text
            $options = $item->getProductOptions();
            $variantText = '';
            if (isset($options['options'])) {
                foreach ($options['options'] as $option) {
                    $variantText .= ", {$option['label']}: {$option['print_value']}";
                }
            }

            // Concat description
            $description = "{$item['name']} {$variantText}";

            $items[] = new ShipmentItem(
                $description,
                $item['sku'],
                '',
                $item['qty_ordered'],
                'pcs',
                ($item['weight'] * $weightUnit),
                $item['sku'],
                $item['price'],
                $item->getTaxPercent(),
                $item['item_id']
            );
        }

        // Billing Address
        $billAdr = new ShipmentAddress();
        $billAdr->Address1 = $mageBilling['street'];
        $billAdr->Address2 = '';
        $billAdr->City = $mageBilling['city'];
        $billAdr->ContactName2 = self::spaceSeparator(
            $mageBilling['firstname'],
            $mageBilling['middlename'],
            $mageBilling['lastname']
        );
        $billAdr->ContactName = $mageBilling['company'];
        $billAdr->CountryCode = $mageBilling['country_id'];
        $billAdr->EMail = $data['customer_email'];
        $billAdr->Phone = $mageBilling['telephone'];
        $billAdr->ZIP = trim($mageBilling['postcode']);


        // Delivery Address
        $delivAdr = new ShipmentAddress();
        $delivAdr->Address1 = $mageDelivery['street'];
        $delivAdr->Address2 = '';
        $delivAdr->City = $mageDelivery['city'];
        $delivAdr->ContactName2 = self::spaceSeparator(
            $mageDelivery['firstname'],
            $mageDelivery['middlename'],
            $mageDelivery['lastname']
        );
        $delivAdr->ContactName = $mageDelivery['company'];
        $delivAdr->CountryCode = $mageDelivery['country_id'];
        $delivAdr->EMail = $data['customer_email'];
        $delivAdr->Phone = $mageDelivery['telephone'];
        $delivAdr->ZIP = trim($mageDelivery['postcode']);


        // Create the shipment
        $shipment = new Shipment();
        $shipment->BillingAddress = $billAdr;
        $shipment->DeliveryAddress = $delivAdr;
        if (!$reProcess) {
            $shipment->DynamicAddress = $delivAdr;
        }
        $shipment->Items = $items;
        $shipment->Weight = ($data['weight'] * $weightUnit);
        $shipment->ExtRef = $data['increment_id'];
        $shipment->ShippingRate = $rateId;
        $shipment->SubTotalPrice = $data['base_subtotal'];
        $shipment->TotalPrice = $data['base_grand_total'];
        $shipment->Currency = $data['base_currency_code'];

        $method = $data['shipping_method'];
        $methodExpl = explode('.', $method);

        /** @var Mage_Core_Helper_Data $helper */
        $helper = Mage::helper('core');
        if (count($methodExpl) == 5 && preg_match('/DYNAMIC/', $method)) {

            $decoded = json_decode(base64_decode($methodExpl[4]));

            $pupAdr = new ShipmentAddress();
            $pupAdr->Address1 = $decoded->street;
            $pupAdr->Address2 = '';
            $pupAdr->City = $decoded->city;
            $pupAdr->ContactName2 = self::spaceSeparator(
                $mageDelivery['firstname'],
                $mageDelivery['middlename'],
                $mageDelivery['lastname']
            );
            $pupAdr->ContactName = $decoded->name;
            $pupAdr->CountryCode = $decoded->country;
            $pupAdr->EMail = $mageDelivery['email'];
            $pupAdr->Phone = $mageDelivery['telephone'];
            $pupAdr->ZIP = $decoded->zip;

            $shipment->DynamicAddress = $pupAdr;
            $shipment->custom_pickup_identifier = $decoded->id;
        }

        $api = self::getAPI($magentoOrder->getStore());
        if ($reProcess) {
            // Send the order
            $responseBody = $api->updateShipment($shipment);
        } else {
            // Send the order
            $responseBody = $api->createShipment($shipment);
        }

        if (is_string($responseBody)) {
            $responseData = $helper->jsonDecode($responseBody);
        } else {
            $responseData = null;
        }

        if (is_array($responseData) && isset($responseData['result'])) {
            $result = strtolower($responseData['result']);
        } else {
            $result = null;
        }

        if ($reProcess) {
            Mage::dispatchEvent('webshipr_order_reprocess_after', array(
                'order'         => $magentoOrder,
                'result'        => $result,
                'response_body' => $responseBody,
            ));
        } else {
            $webshiprId = is_array($responseData) && isset($responseData['id']) ? (int)$responseData['id'] : null;
            Mage::dispatchEvent('webshipr_order_process_after', array(
                'order'         => $magentoOrder,
                'result'        => $result,
                'webshipr_id'   => $webshiprId,
                'response_body' => $responseBody,
            ));
        }

        return $responseBody;
    }

    /**
     * @param Mage_Sales_Model_Order $order
     * @return Mage_Sales_Model_Order_item[]
     */
    protected static function getShipmentItems($order)
    {
        $shipmentItems = array();
        /** @var Mage_Sales_Model_Order_item $item */
        foreach ($order->getAllItems() as $item) {
            // Test if this is a child item
            if ($item->getParentItem()) {
                $addItem = $item->isChildrenCalculated();
            } else { // This is a single or parent item
                $addItem = !$item->isChildrenCalculated();
            }

            if ($addItem) {
                $shipmentItems[] = $item;
            }
        }

        return $shipmentItems;
    }

    public static function isWebshiprRate($strRate)
    {
        return (preg_match('/WS/', $strRate) ? true : false);
    }

    public static function getWebshiprRateIDFromMethod($strRate)
    {
        $arr = explode('.', $strRate);
        return $arr[1];
    }

    public static function isCountryAccepted($rate, $curCountry)
    {
        $result = false;
        foreach ($rate->accepted_countries as $country) {
            if ($country->code == $curCountry || $country == 'ALL') {
                $result = true;
            }
        }
        return $result;
    }

    /**
     * @param Mage_Core_Model_Store $store
     * @return Webshipr_Shipping_Model_WebshiprAPI
     */
    public static function getAPI(Mage_Core_Model_Store $store)
    {
        /** @var Webshipr_Shipping_Model_WebshiprAPI $apiModel */
        $apiModel = Mage::getModel('webshipr_shipping/webshiprAPI');
        $apiModel->init($store);
        return $apiModel;
    }

    /**
     * Implode list of params with space but don't include empty values
     *
     * @param mixed $text,...
     *
     * @return string
     */
    protected static function spaceSeparator()
    {
        $args = func_get_args();
        return implode(' ', array_filter($args, function ($arg) {
            return is_numeric($arg) || !empty($arg);
        }));
    }
}
