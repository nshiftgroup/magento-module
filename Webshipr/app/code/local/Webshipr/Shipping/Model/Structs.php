<?php

class ShipmentItem
{
    public $Description;
    public $ProductName;
    public $ProductNo;
    public $Quantity;
    public $UOM;
    public $Weight;
    public $Location;
    public $Price;
    public $TaxPercent;
    public $ExtRef;

    public function __construct($Description, $ProductName, $ProductNo, $Quantity, $UOM, $Weight, $Location, $Price, $TaxPercent, $ExtRef)
    {
        $this->Description = $Description;
        $this->ProductName = $ProductName;
        $this->ProductNo = $ProductNo;
        $this->Quantity = $Quantity;
        $this->UOM = $UOM;
        $this->Weight = $Weight;
        $this->Location = $Location;
        $this->Price = $Price;
        $this->TaxPercent = $TaxPercent;
        $this->ExtRef = $ExtRef;
    }
}


class Shipment
{
    public $BillingAddress;
    public $DeliveryAddress;
    public $DynamicAddress;
    public $Items;
    public $WebshopID;
    public $Weight;
    public $ExtRef;
    public $ShippingRate;
    public $SubTotalPrice;
    public $TotalPrice;
    public $Currency;
    public $custom_pickup_identifier;
}

class ShipmentAddress
{
    public $Address1;
    public $Address2;
    public $City;
    public $ContactName;
    public $ContactName2;
    public $CountryCode;
    public $EMail;
    public $Phone;
    public $ZIP;
    public $province;
}
