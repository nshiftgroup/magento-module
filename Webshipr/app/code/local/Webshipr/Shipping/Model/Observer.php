<?php

require_once("WebshiprGlue.php");

class Webshipr_Shipping_Model_Observer
{

    public function configurationUpdated(Varien_Event_Observer $observer)
    {

        if ($observer->getData("section") == 'webshipr') {

            $scope = Mage::getSingleton('adminhtml/config_data')->getScope();

            $storeParam = $observer->getData('store');
            $websiteParam = $observer->getData('website');


            $api = $this->getAPI();
            if ($scope === 'stores') // store level
            {
                $store = Mage::app()->getStore($observer->getData('store'));
                $apiHost = $store->getConfig($api::XML_PATH_ENDPOINT);
                $apiToken = $store->getConfig($api::XML_PATH_TOKEN);
                $baseUrlStore = $store;
                $successString = 'The store is connected as: %s';
            } elseif ($scope === 'websites') // website level
            {
                $website = Mage::app()->getWebsite($observer->getData('website'));
                $apiHost = $website->getConfig($api::XML_PATH_ENDPOINT);
                $apiToken = $website->getConfig($api::XML_PATH_TOKEN);
                $baseUrlStore = $website->getDefaultStore();
                $successString = 'The website is connected as: %s';
            } else // default level
            {
                $store = Mage::app()->getStore(Mage_Core_Model_Store::ADMIN_CODE);
                $apiHost = $store->getConfig($api::XML_PATH_ENDPOINT);
                $apiToken = $store->getConfig($api::XML_PATH_TOKEN);
                $baseUrlStore = Mage::app()->getDefaultStoreView();
                $successString = 'Magento is connected as: %s';
            }
            $baseUrl = $baseUrlStore->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_LINK);
            $isConnectionOK = $api->checkConnection(
                $baseUrl,
                $apiHost,
                $apiToken
            );

            $helper = Mage::helper('webshipr_shipping');
            if ($isConnectionOK) {
                Mage::getSingleton("core/session")->addSuccess(
                    $helper->__($successString, $isConnectionOK->Shop_name)
                );
            } else {
                Mage::getSingleton("core/session")
                    ->addError($helper->__('Wrong API key. Please retrieve a valid API token from webshipr panel'));
            }
        }
    }

    public function autoProcess($observer)
    {
        /** @var Mage_Sales_Model_Order $orderMain */
        $orderMain = $observer->getEvent()->getOrder();
        $orderStatus = $orderMain->getStatus();
        /** @var Webshipr_Shipping_Model_WebshiprGlue $glue */
        $glue = Mage::getModel('webshipr_shipping/webshiprGlue');
        if (Mage::getStoreConfig('webshipr/webshipr/webshipr_orderstatus', $orderMain->getStore())) {

            $orderStatuses = Mage::getStoreConfig('webshipr/webshipr/webshipr_orderstatus', $orderMain->getStore());
            $statusArray = explode(",", $orderStatuses);
            if (in_array($orderStatus, $statusArray)) {
                if (Mage::getStoreConfig('webshipr/webshipr/webshipr_autoprocess', $orderMain->getStore())) {
                    $rateId = 0;
                    if ($glue->isWebshiprRate($orderMain["shipping_method"])) {
                        $rateId = $glue->getWebshiprRateIDFromMethod($orderMain["shipping_method"]);
                    }
                    $glue->processOrder($orderMain, $rateId);
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
    }

    /**
     * @return Webshipr_Shipping_Model_WebshiprAPI
     */
    protected function getAPI()
    {
        /** @var Webshipr_Shipping_Model_WebshiprAPI $apiModel */
        $apiModel = Mage::getModel('webshipr_shipping/webshiprAPI');
        return $apiModel;
    }
}
