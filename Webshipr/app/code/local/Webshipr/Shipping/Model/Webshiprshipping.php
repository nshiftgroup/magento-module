<?php
require_once("WebshiprGlue.php");

class Webshipr_Shipping_Model_Webshiprshipping extends Mage_Shipping_Model_Carrier_Abstract
    implements Mage_Shipping_Model_Carrier_Interface
{
    protected $_code = 'webshipr';

    public function collectRates(Mage_Shipping_Model_Rate_Request $request)
    {

        /**
         * Get adapter for getting rates
         * @var Webshipr_Shipping_Model_WebshiprGlue $glue
         *
         * Get API Instance for parcel shops
         * @var Webshipr_Shipping_Model_WebshiprAPI $api
         */
        $glue = Mage::getModel('webshipr_shipping/webshiprGlue');
        $api = $glue->getAPI(Mage::app()->getStore($request->getStoreId()));

        // Retrieve weight unit setting.
        $weightUnit = Mage::getStoreConfig(
            'carriers/webshipr/weight_unit',
            $request->getStoreId()
        );

        // Destination stuff
        $city = $request->getDestCity();
        $zip = $request->getDestPostcode();
        $country = $request->getDestCountryId();
        $street = $request->getDestStreet();

        foreach ($request->getAllItems() as $item) {
            $request->setPackageValue($request->getPackageValue() + $item->getTaxAmount());
        }

        // Get webshipr API instance
        $webshiprRates = $api->getShippingRates($request->getPackageValue());


        $result = Mage::getModel('shipping/rate_result');

        if ($webshiprRates) {

            // Loop through rates from webshipr.
            foreach ($webshiprRates as $rMethod) {
                $methodAvailable = false;

                // Get total weight
                $totalWiegth = 0;
                foreach ($request->getAllItems() as $item) {
                    $totalWiegth += $item->getWeight() * $item->getQty();
                }

                // Check if order satisfies the rates limits.
                $mappedCountries = array_values(array_map(function ($n) {
                    return $n->code;
                }, $rMethod->accepted_countries));

                if (($totalWiegth * $weightUnit) >= $rMethod->min_weight &&
                    ($totalWiegth * $weightUnit) <= $rMethod->max_weight &&
                    in_array($country, $mappedCountries))
                    $methodAvailable = true;


                if ($methodAvailable) {

                    // Check if we should resolve drop points for this method
                    if ($rMethod->dynamic_pickup) {

                        $shops = $api->getShopsByRateAndAddress($street, $zip, $country, $rMethod->id);
                        if (isset($shops->data) && is_array($shops->data)) {

                            // Figure out whats the max of drop points
                            $puplimit = (int)Mage::getStoreConfig('webshipr/webshipr/webshipr_puplimit', $request->getStoreId());
                            if ((int)$puplimit > 0) {
                                $puplimit = (int)$puplimit;
                            } else {
                                $puplimit = 10;
                            }

                            foreach ($shops->data as $index => $shop) {

                                // Respect PUP limit
                                if (((int)$index + 1) > $puplimit)
                                    break;

                                // Generate Base 64 representation of drop point
                                $arrData = array('id' => $shop->id, 'name' => $shop->name, 'street' => $shop->street, 'zip' => $shop->zip, 'city' => $shop->city, 'country' => $shop->country);
                                $base64Data = base64_encode(json_encode($arrData));

                                // Create new instance of method rate
                                $method = Mage::getModel('shipping/rate_result_method');

                                // Record carrier information
                                $method->setCarrier($this->_code);

                                $method->setCarrierTitle($rMethod->carrier_name);
                                $method->setCarrierName('');

                                // Record method information
                                $method->setMethod("WS." . $rMethod->id . ".DYNAMIC." . $shop->id . "." . $base64Data);
                                $method->setMethodTitle($rMethod->name . " - " . $shop->name . ", " . $shop->street . ", " . $shop->zip . " " . $shop->city);

                                $method->setCost($rMethod->cost);

                                $shippingPrice = $this->getShippingPrice($request, $rMethod->price);

                                $method->setPrice($shippingPrice);
                                $method->setDynamicPickup($rMethod->dynamic_pickup);
                                $method->setWsRateId($rMethod->id . "-" . $shop->id);


                                // add this rate to the result
                                $result->append($method);

                            }

                        }


                    } else {
                        // create new instance of method rate
                        $method = Mage::getModel('shipping/rate_result_method');

                        // record carrier information
                        $method->setCarrier($this->_code);

                        $method->setCarrierTitle($rMethod->carrier_name);
                        $method->setCarrierName('');

                        // record method information
                        $method->setMethod("WS." . $rMethod->id . "." . "STATIC");
                        $method->setMethodTitle($rMethod->name);
                        $method->setCost($rMethod->cost);

                        $shippingPrice = $this->getShippingPrice($request, $rMethod->price);
                        $method->setPrice($shippingPrice);
                        $method->setDynamicPickup($rMethod->dynamic_pickup);
                        $method->setWsRateId($rMethod->id);

                        // add this rate to the result
                        $result->append($method);
                    }
                }
            }
        }


        return $result;
    }


    protected function getShippingPrice(Mage_Shipping_Model_Rate_Request $request, $ratePrice)
    {


        $freeBoxes = 0;
        if ($request->getAllItems()) {
            foreach ($request->getAllItems() as $item) {

                if ($item->getProduct()->isVirtual() || $item->getParentItem()) {
                    continue;
                }

                if ($item->getHasChildren() && $item->isShipSeparately()) {
                    foreach ($item->getChildren() as $child) {
                        if ($child->getFreeShipping() && !$child->getProduct()->isVirtual()) {
                            $freeBoxes += $item->getQty() * $child->getQty();
                        }
                    }
                } elseif ($item->getFreeShipping()) {
                    $freeBoxes += $item->getQty();
                }
            }
        }
        $this->setFreeBoxes($freeBoxes);

        $shippingPrice = $this->getFinalPriceWithHandlingFee($ratePrice);

        if ($request->getFreeShipping() || ($request->getPackageQty() == $this->getFreeBoxes())) {
            $shippingPrice = '0.00';
        }

        return $shippingPrice;
    }

    public function getAllowedMethods()
    {
        return array($this->_code => 'Automated shipping');
    }
}
