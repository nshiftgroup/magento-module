<?php
include_once 'Structs.php';

class Webshipr_Shipping_Model_WebshiprAPI
{

    const XML_PATH_ENDPOINT = 'webshipr/webshipr/webshipr_endpoint';
    const XML_PATH_TOKEN = 'webshipr/webshipr/webshipr_token';

    protected $_apiToken;
    protected $_baseHost;

    public function init(Mage_Core_Model_Store $store = null)
    {
        $store = Mage::app()->getStore($store);
        if ($store->isAdmin()) {
            throw new LogicException('Admin store scope can not initiate a Webshipr API');
        }
        $this->_baseHost = $store->getConfig(self::XML_PATH_ENDPOINT);
        $this->_apiToken = $store->getConfig(self::XML_PATH_TOKEN);
    }

    public function orderExists($orderNo)
    {
        $result = $this->postData("order_exists", array("ExtRef" => $orderNo));

        if ($result->exists) {
            return true;
        } else {
            return false;
        }
    }

    public function getPostDKShops($country, $street, $number, $postal)
    {
        $data = array("postal_code" => $postal, "country" => $country, "street" => $street, "number" => $number);
        return $this->postData("post_dk_shops", $data)->servicePointInformationResponse;
    }

    protected function postData($resource, $data)
    {

        $ch = curl_init($this->_baseHost . "/API/" . $resource);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
            'Authorization: Token token="' . $this->_apiToken . '"'));
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        return json_decode(curl_exec($ch));
    }

    protected function postDataV2($path, $data)
    {
        $ch = curl_init($this->_baseHost . '/APIV2/' . $path);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
            'Authorization: Token token="' . $this->_apiToken . '"'));
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        return json_decode(curl_exec($ch));
    }

    public function getOrder($orderNo)
    {
        return $this->postData("get_order", array("ExtRef" => $orderNo));
    }

    public function setAndProcessOrder($orderNo, $rateId)
    {
        return $this->postData("set_rate", array("ExtRef" => $orderNo, "new_rate" => $rateId));
    }


    public function updateShipment($shipment)
    {

        $datatopost = array(
            "billing_address" => (array)$shipment->BillingAddress,
            "delivery_address" => (array)$shipment->DeliveryAddress,
            "dynamic_address" => (array)$shipment->DynamicAddress,
            "process" => true,
            "items" => (array)$shipment->Items,
            "ExtRef" => $shipment->ExtRef,
            "Weight" => $shipment->Weight,
            "ShippingRate" => $shipment->ShippingRate,
            "SubTotalPrice" => $shipment->SubTotalPrice,
            "TotalPrice" => $shipment->TotalPrice,
            "Currency" => $shipment->Currency,
            "custom_pickup_identifier" => $shipment->custom_pickup_identifier
        );

        $ch = curl_init($this->_baseHost . "/API/update_shipment");
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
            'Authorization: Token token="' . $this->_apiToken . '"'));
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($datatopost));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $returndata = curl_exec($ch);
        return $returndata;
    }

    public function createShipment($shipment)
    {

        $datatopost = array(
            "billing_address" => (array)$shipment->BillingAddress,
            "delivery_address" => (array)$shipment->DeliveryAddress,
            "dynamic_address" => (array)$shipment->DynamicAddress,
            "items" => (array)$shipment->Items,
            "ExtRef" => $shipment->ExtRef,
            "Weight" => $shipment->Weight,
            "ShippingRate" => $shipment->ShippingRate,
            "SubTotalPrice" => $shipment->SubTotalPrice,
            "TotalPrice" => $shipment->TotalPrice,
            "Currency" => $shipment->Currency,
            "custom_pickup_identifier" => $shipment->custom_pickup_identifier
        );
        $ch = curl_init($this->_baseHost . "/API/create_shipment");
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
            'Authorization: Token token="' . $this->_apiToken . '"'));
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($datatopost));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $returndata = curl_exec($ch);

        return $returndata;
    }

    /**
     * @param string $checkUrl
     * @param string $apiHost
     * @param string $apiToken
     * @return bool
     */
    public function checkConnection($checkUrl = '', $apiHost, $apiToken)
    {
        $ch = curl_init("{$apiHost}/API/check_connection");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
            'Authorization: Token token="' . $apiToken . '"'));
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode(array("base_host" => $checkUrl)));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $returndata = curl_exec($ch);
        $res = json_decode($returndata);

        if ($res) {
            return $res;
        } else {
            return false;
        }
    }

    public function getShippingRates($total = 0)
    {
        $ch = curl_init($this->_baseHost . "/API/shipping_rates");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
            'Authorization: Token token="' . $this->_apiToken . '"'));
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode(array("order_total" => $total)));
        $returndata = curl_exec($ch);
        $res = json_decode($returndata);

        if ($res) {
            return $res;
        } else {
            return false;
        }
    }

    public static function generateAjaxToken($length = 10)
    {
        $token = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
        Mage::getModel('checkout/session')->setWsAjaxToken($token);
        return $token;
    }

    /* Version 2 of PUP API */


    public function getShopsByCarrierAndZip($zip, $carrier)
    {
        return $this->postDataV2('PUP/pupShopsByZip', array("carrier" => $carrier, "zip" => $zip));
    }

    public function getShopsByRateAndZip($zip, $rateId)
    {
        return $this->postDataV2('PUP/pupShopsByZip', array("rate_id" => $rateId, "zip" => $zip));
    }

    public function getShopsByRateAndAddress($address, $zip, $country, $rateId)
    {
        return $this->postDataV2("PUP/pupShopsByAddress", array("rate_id" => $rateId,
            "address" => $address,
            "zip" => $zip,
            "country" => $country));
    }

    public function getShopsByCarrierAndAddress($address, $zip, $country, $carrier)
    {
        return $this->postDataV2("PUP/pupShopsByAddress", array("carrier" => $carrier,
            "address" => $address,
            "zip" => $zip,
            "country" => $country));
    }

    /* End V2 PUP API */
}
