
Webshipr for magento - Plug 'n Play

This module is intended for customers who wants a simple solution, with all the functionality. This module offers droppoints, but not any map in the checkout, as this often needs customizations.

To install the module, simply upload "app" to the root of your magento installation. 

After files are copied to the folder, please empty magento cache, log out and login again.

Now go to System => Configuration => Webshipr  and fill in token, and preferred settings.

Remember allways to make a backup before installing a module.
